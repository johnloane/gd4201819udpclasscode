#include "SocketWrapperPCH.hpp"
void DoServiceLoop(UDPSocketPtr clientSocket);
void PrintOptions();
void GetChoice(string &choice);
void SendDataToServer(UDPSocketPtr clientSocket, char* input);
int ConvertIPToInt(std::string ipString);
void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress senderAddress, int bytesReceived, bool &serviceRunning);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress senderAddress, bool &serviceRunning);
void NaivelySendPlayer(UDPSocketPtr clientSocket, const Player* player);
void SendComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendCompressedPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext);
const int32_t MAXPACKETSIZE = 1300;
const uint8_t SENDCOMPLEXPLAYER = 5;
const uint8_t NAIVELYSENDPLAYER = 4;
const uint8_t SENDCOMPRESSEDPLAYER = 6;
const uint8_t SENDWORLD = 7;

int main()
{
	SocketUtil::StaticInit();
	UDPSocketPtr clientSocket = SocketUtil::CreateUDPSocket(INET);
	clientSocket->SetNonBlockingMode(false);
	DoServiceLoop(clientSocket);
}

void DoServiceLoop(UDPSocketPtr clientSocket)
{
	bool serviceRunning = true;
	string choice;
	char receiveBuffer[MAXPACKETSIZE];
	//the next three lines set the receive buffer to null
	char *begin = receiveBuffer;
	char *end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);
	LinkingContext gameContext;

	unique_ptr<Player> john = std::make_unique<Player>();

	unique_ptr<ComplexPlayer> complexJohn = std::make_unique<ComplexPlayer>();

	SocketAddress senderAddress;
	int bytesReceived = 0;

	while (serviceRunning)
	{
		PrintOptions();
		GetChoice(choice);
		SendDataToServer(clientSocket, (char *)choice.c_str());
		if (std::stoi(choice) == NAIVELYSENDPLAYER) {
			NaivelySendPlayer(clientSocket, john.get());
		}
		else if (std::stoi(choice) == SENDCOMPLEXPLAYER)
		{
			SendComplexPlayer(clientSocket, complexJohn.get());
		}
		else if (std::stoi(choice) == SENDCOMPRESSEDPLAYER)
		{
			SendCompressedPlayer(clientSocket, complexJohn.get());
		}
		else if (std::stoi(choice) == SENDWORLD)
		{
			SendWorld(clientSocket, complexJohn.get(), gameContext);
		}
		
		ReceiveDataFromServer(clientSocket, receiveBuffer, senderAddress, bytesReceived, serviceRunning);
	}
}

void PrintOptions()
{
	std::cout << "Please enter: " << std::endl;
	std::cout << "1) To use the ECHO service: " << std::endl;
	std::cout << "2) To use the DATEANDTIME service: " << std::endl;
	std::cout << "3) To use the STATS service: " << std::endl;
	std::cout << "4) To send the player naively: " << std::endl;
	std::cout << "5) To send the complex player using byte stream: " << std::endl;
	std::cout << "6) To send the compressed player using bit stream: " << std::endl;
	std::cout << "7) To send world using object replication: " << std::endl;
	std::cout << "8) To use the QUIT service: " << std::endl;
}

void GetChoice(string &choice)
{
	std::getline(std::cin, choice);
}

void SendDataToServer(UDPSocketPtr clientSocket, char* input)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(input, strlen(input), serverAddress);
}

int ConvertIPToInt(std::string ipString)
{
	int intIP = 0;
	for (int i = 0; i < ipString.length(); ++i)
	{
		if (ipString[i] == '.')
		{
			ipString[i] = ' ';
		}
	}
	vector<int> arrayTokens;
	std::stringstream ss(ipString);
	int temp;
	while (ss >> temp)
	{
		arrayTokens.emplace_back(temp);
	}
	for (int i = 0; i < arrayTokens.size(); ++i)
	{
		intIP += (arrayTokens[i] << ((3 - i) * 8));
	}
	return intIP;
}

void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress senderAddress, int bytesReceived, bool &serviceRunning)
{
	bytesReceived = clientSocket->ReceiveFrom(receiveBuffer, MAXPACKETSIZE, senderAddress);
	if (bytesReceived > 0)
	{
		ProcessReceivedData(receiveBuffer, bytesReceived, senderAddress, serviceRunning);
	}
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress senderAddress, bool &serviceRunning)
{
	if (strcmp("QUIT", receiveBuffer) == 0)
	{
		std::cout << "Server says we have to shut down..." << std::endl;
		serviceRunning = false;
	}
	std::cout << "Receive Buffer size " << strlen(receiveBuffer) << std::endl;
	std::cout << "Got " << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;
}

void NaivelySendPlayer(UDPSocketPtr clientSocket, const Player* player)
{
	std::cout << "Player has size: " << sizeof(Player) << std::endl;
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(reinterpret_cast<const char*>(player), sizeof(Player), serverAddress);
	std::cout << "Sent player of size " << sizeof(Player) << "to the server" << std::endl;
}

void SendComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryStream outStream;
	player->Write(outStream);
	int bytesSent = clientSocket->SendTo(outStream.GetBufferPtr(), outStream.GetLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendCompressedPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext)
{
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	//First send the type of the packet
	PacketType orPacket = PT_ReplicationData;
	outBitStream.WriteBits(&orPacket, 2);
	//Second send the id of the object
	uint32_t networkId = gameContext.GetNetworkId(player, true);
	std::cout << "Id is: " << networkId << std::endl;
	outBitStream.WriteBits(networkId, 32);
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

