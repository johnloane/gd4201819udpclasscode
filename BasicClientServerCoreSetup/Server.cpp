#include "SocketWrapperPCH.hpp"

void DoServiceLoop(UDPSocketPtr serverSocket);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool &serviceRunning, LinkingContext& gameContext);
std::string ReturnCurrentDateAndTime();
int ConvertIPToInt(std::string ipString);
void NaivelyReceivePlayer(UDPSocketPtr serverSocket);
void ReceiveComplexPlayer(UDPSocketPtr serverSocket);
void ReceiveCompressedPlayer(UDPSocketPtr serverSocket);
void ReceiveWorld(UDPSocketPtr serverSocket, LinkingContext& gameContext);
enum Choice{ECHO = 1, DATEANDTIME = 2, STATS = 3, NAIVELYSENDPLAYER = 4, SENDCOMPLEXPLAYER = 5, SENDCOMPRESSEDPLAYER= 6, SENDWORLD = 7, QUIT = 8};
const int32_t MAXPACKETSIZE = 1300;

int main()
{
	SocketUtil::StaticInit();
	UDPSocketPtr serverSocket = SocketUtil::CreateUDPSocket(INET);
	SocketAddress serverAddress = SocketAddress(ConvertIPToInt("127.0.0.1"), 50005);
	serverSocket->Bind(serverAddress);
	serverSocket->SetNonBlockingMode(false);
	DoServiceLoop(serverSocket);
}

void DoServiceLoop(UDPSocketPtr serverSocket)
{
	bool serviceRunning = true;
	char receiveBuffer[MAXPACKETSIZE];
	//the next three lines set the receive buffer to null
	char *begin = receiveBuffer;
	char *end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);

	SocketAddress senderAddress;
	int bytesReceived = 0;
	int requests = 0;

	LinkingContext gameContext;

	while (serviceRunning)
	{
		bytesReceived = serverSocket->ReceiveFrom(receiveBuffer, sizeof(receiveBuffer), senderAddress);
		if (bytesReceived > 0)
		{
			requests++;
			ProcessReceivedData(receiveBuffer, bytesReceived, senderAddress, serverSocket, requests, serviceRunning, gameContext);
		}
	}
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool &serviceRunning, LinkingContext& gameContext)
{
	std::cout << "Got " << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;

	char responseData[MAXPACKETSIZE] = "";
	int choice = atoi(receiveBuffer);
	std::cout << "Choice: " << choice << std::endl;

	std::string currentDateAndTime = "";
	std::string requestString = "";
	std::string quitString = "QUIT";
	unique_ptr<ComplexPlayer> complexJohn = std::make_unique<ComplexPlayer>();

	switch (choice)
	{
	case ECHO:
		std::cout << "ECHO request" << std::endl;
		strcpy_s(responseData, receiveBuffer);
		break;
	case DATEANDTIME:
		std::cout << "DATEANDTIME request" << std::endl;
		currentDateAndTime = ReturnCurrentDateAndTime();
		currentDateAndTime.copy(responseData, currentDateAndTime.length(), 0);
		break;
	case STATS:
		std::cout << "STATS request" << std::endl;
		requestString = std::to_string(requests);
		requestString.copy(responseData, requestString.length());
		break;
	case NAIVELYSENDPLAYER:
		NaivelyReceivePlayer(serverSocket);
		std::cout << "NAIVELYSEND request" << std::endl;
		requestString = "Thanks for the player";
		requestString.copy(responseData, requestString.length(), 0);
		break;
	case SENDCOMPLEXPLAYER:
		std::cout << "SENDCOMPLEXPLAYER request" << std::endl;
		ReceiveComplexPlayer(serverSocket);
		break;
	case SENDCOMPRESSEDPLAYER:
		std::cout << "SENDCOMPRESSEDPLAYER request" << std::endl;
		ReceiveCompressedPlayer(serverSocket);
		break;
	case SENDWORLD:
		std::cout << "SENDWORLD request" << std::endl;
		ReceiveWorld(serverSocket, gameContext);
		break;
	case QUIT:
		std::cout << "QUIT request" << std::endl;
		quitString.copy(responseData, quitString.length());
		serviceRunning = false;
		break;
	}
	int byteSent = serverSocket->SendTo(responseData, sizeof(responseData), socketAddress);
}

std::string ReturnCurrentDateAndTime()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
	return ss.str();
}

int ConvertIPToInt(std::string ipString)
{
	int intIP = 0;
	for (int i = 0; i < ipString.length(); ++i)
	{
		if (ipString[i] == '.')
		{
			ipString[i] = ' ';
		}
	}
	vector<int> arrayTokens;
	std::stringstream ss(ipString);
	int temp;
	while (ss >> temp)
	{
		arrayTokens.emplace_back(temp);
	}
	for (int i = 0; i < arrayTokens.size(); ++i)
	{
		intIP += (arrayTokens[i] << ((3 - i) * 8));
	}
	return intIP;
}

void NaivelyReceivePlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<Player> receiver = std::make_unique<Player>();
	SocketAddress socketAddress;

	int bytesReceived = serverSocket->ReceiveFrom(reinterpret_cast<char*>(receiver.get()), sizeof(Player), socketAddress);
	std::cout << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	receiver->toString();

}

void ReceiveComplexPlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));

	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived));
	receiver->Read(stream);

	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}

void ReceiveCompressedPlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));

	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived*8));
	receiver->ReadBits(stream);

	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}

void ReceiveWorld(UDPSocketPtr serverSocket, LinkingContext& gameContext)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));

	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived * 8));
	PacketType receivePacket;
	stream.ReadBits(&receivePacket, 2);
	uint32_t networkId;
	stream.ReadBits(&networkId, 32);
	if (gameContext.GetGameObject(networkId) == nullptr)
	{
		std::cout << "This object does not exist here. Need to create it" << std::endl;
		int newId = gameContext.GetNetworkId(receiver.get(), true);
		std::cout << "Object now has id: " << newId << std::endl;
	}
	else {
		std::cout << "Object already exists. Can just copy the data into it" << std::endl;
	}

	receiver->ReadBits(stream);

	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}

