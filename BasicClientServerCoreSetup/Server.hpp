/* Client can contact the server and offers three services
ECHO - just repeats whatever you send
DateAndTime - sends back the date and time
Stats - sends back how many queries the server has handled*/

class Server
{
public:
	void DoServiceLoop(UDPSocketPtr serverSocket);
	void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool &serviceRunning);
	std::string ReturnCurrentDateAndTime();
};
