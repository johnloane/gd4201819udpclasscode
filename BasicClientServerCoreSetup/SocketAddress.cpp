#include "SocketWrapperPCH.hpp"

SocketAddress::SocketAddress(uint32_t inAddress, uint16_t inPort)
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = htonl(inAddress);
	GetAsSockAddrIn()->sin_port = htons(inPort);
}

SocketAddress::SocketAddress(const sockaddr& inSockAddr)
{
	memcpy(&mSockAddr, &inSockAddr, sizeof(sockaddr));
}

SocketAddress::SocketAddress()
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = INADDR_ANY;
	GetAsSockAddrIn()->sin_port = 0;
}

uint32_t& SocketAddress::GetIP4Ref() {
#if _WIN32
	return *reinterpret_cast<uint32_t*>(&GetAsSockAddrIn()->sin_addr.S_un.S_addr);
#else
	return (GetAsSockAddrIn()->sin_addr.S_addr);
#endif
}

sockaddr_in* SocketAddress::GetAsSockAddrIn() {
	return reinterpret_cast<sockaddr_in*>(&mSockAddr);
}

const uint32_t& SocketAddress::GetIP4Ref() const
{
#if _WIN32
	return *const_cast<uint32_t*>(reinterpret_cast< const uint32_t*>(&GetAsSockAddrIn()->sin_addr.S_un.S_addr));
#else
	return (GetAsSockAddrIn()->sin_addr.S_addr);
#endif
}

bool SocketAddress::operator==(const SocketAddress& inOther) const
{
	return (mSockAddr.sa_family == AF_INET && GetAsSockAddrIn()->sin_port == inOther.GetAsSockAddrIn()->sin_port)
		&& (GetIP4Ref() == inOther.GetIP4Ref());
}

size_t SocketAddress::GetHash() const
{
	return (GetIP4Ref() | ((static_cast<uint32_t>(GetAsSockAddrIn()->sin_port)) << 13) | mSockAddr.sa_family);
}

uint32_t SocketAddress::GetSize() const
{
	return sizeof(sockaddr);
}

string SocketAddress::ToString() const
{
#if _WIN32
	const sockaddr_in* s = GetAsSockAddrIn();
	char destinationbuffer[128];
	InetNtop(s->sin_family, const_cast<in_addr*>(&s->sin_addr), destinationbuffer, sizeof(destinationbuffer));
	return StringUtils::Sprintf("%s:%d", destinationbuffer, ntohs(s->sin_port));
#else
	return string("Haven't written the linux version yet");
#endif
}

const sockaddr_in* SocketAddress::GetAsSockAddrIn() const
{
	return reinterpret_cast<const sockaddr_in*>(&mSockAddr);
}
